package pt.lighthouselabs.obd.reader.io;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.inject.Inject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import pt.lighthouselabs.obd.commands.ObdCommand;
import pt.lighthouselabs.obd.commands.Parameters;
import pt.lighthouselabs.obd.commands.engine.EngineRPMObdCommand;
import pt.lighthouselabs.obd.commands.protocol.EchoOffObdCommand;
import pt.lighthouselabs.obd.commands.protocol.LineFeedOffObdCommand;
import pt.lighthouselabs.obd.commands.protocol.ObdResetCommand;

import pt.lighthouselabs.obd.commands.protocol.OdbRawCommand;
import pt.lighthouselabs.obd.commands.protocol.SelectProtocolObdCommand;
import pt.lighthouselabs.obd.commands.protocol.TimeoutObdCommand;
import pt.lighthouselabs.obd.commands.temperature.AmbientAirTemperatureObdCommand;
import pt.lighthouselabs.obd.enums.ObdProtocols;
import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.R;
import pt.lighthouselabs.obd.reader.activity.ConfigActivity;
import pt.lighthouselabs.obd.reader.activity.MainActivity;
import pt.lighthouselabs.obd.reader.config.ObdATICommand;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob.ObdCommandJobState;
import pt.lighthouselabs.obd.reader.util.Logger;
import roboguice.service.RoboService;

/**
 * This service is primarily responsible for establishing and maintaining a
 * permanent connection between the device where the application runs and a more
 * OBD Bluetooth interface.
 * <p/>
 * Secondarily, it will serve as a repository of ObdCommandJobs and at the same
 * time the application state-machine.
 */
public class ObdGatewayService extends AbstractGatewayService {

    private static final String TAG = ObdGatewayService.class.getName();
    /*
     * http://developer.android.com/reference/android/bluetooth/BluetoothDevice.html
     * #createRfcommSocketToServiceRecord(java.util.UUID)
     *
     * "Hint: If you are connecting to a Bluetooth serial board then try using the
     * well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB. However if you
     * are connecting to an Android peer then please generate your own unique
     * UUID."
     */
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private final IBinder binder = new ObdGatewayServiceBinder();
    @Inject
    SharedPreferences prefs;

    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;
    private BluetoothSocket sockFallback = null;

    public void startService() {
        Log.d(TAG, "Starting service..");

        // get the remote Bluetooth device
        final String remoteDevice = prefs.getString(ConfigActivity.BLUETOOTH_LIST_KEY, null);
        if (remoteDevice == null || "".equals(remoteDevice)) {
            Toast.makeText(ctx, "No Bluetooth device selected", Toast.LENGTH_LONG).show();

            // log error
            Log.e(TAG, "No Bluetooth device has been selected.");

            // TODO kill this service gracefully
            stopService();
            return;
        }

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        dev = btAdapter.getRemoteDevice(remoteDevice);

    /*
     * TODO clean
     *
     * Get more preferences
     */
        boolean imperialUnits = prefs.getBoolean(ConfigActivity.IMPERIAL_UNITS_KEY,
                false);
        ArrayList<ObdCommand> cmds = ConfigActivity.getObdCommands(prefs);

    /*
     * Establish Bluetooth connection
     *
     * Because discovery is a heavyweight procedure for the Bluetooth adapter,
     * this method should always be called before attempting to connect to a
     * remote device with connect(). Discovery is not managed by the Activity,
     * but is run as a system service, so an application should always call
     * cancel discovery even if it did not directly request a discovery, just to
     * be sure. If Bluetooth state is not STATE_ON, this API will return false.
     *
     * see
     * http://developer.android.com/reference/android/bluetooth/BluetoothAdapter
     * .html#cancelDiscovery()
     */
        Log.d(TAG, "Stopping Bluetooth discovery.");
        btAdapter.cancelDiscovery();

        showNotification("Tap to open OBD-Reader", "Starting OBD connection..", R.drawable.ic_launcher, true, true, false);

        try {
            startObdConnection();
        } catch (Exception e) {
            Log.e(
                    TAG,
                    "There was an error while establishing connection. -> "
                            + e.getMessage()
            );

            // in case of failure, stop this service.
            stopService();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        ((Activity) ctx).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((ObdProgressListener) ctx).addTableRow("Connection", "stopped");
            }
        });


    }

    /**
     * Start and configure the connection to the OBD interface.
     * <p/>
     * See http://stackoverflow.com/questions/18657427/ioexception-read-failed-socket-might-closed-bluetooth-on-android-4-3/18786701#18786701
     *
     * @throws IOException
     */
    private void startObdConnection() throws IOException {
        Log.d(TAG, "Starting OBD connection..");

        try {
            // Instantiate a BluetoothSocket for the remote device and connect it.
            sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();
        } catch (Exception e1) {
            Log.e(TAG, "There was an error while establishing Bluetooth connection. Falling back..", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();
                sock = sockFallback;
            } catch (Exception e2) {
                Log.e(TAG, "Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
                stopService();
                return;
            }
        }
        List<ObdCommand> initCommands = ConfigActivity.getObdCommands(prefs);

        // Let's configure the connection.
        Log.d(TAG, "Queing jobs for connection configuration.." + sock);
        InputStream in = sock.getInputStream();
        Log.d(TAG, "InputStream:" + in);

        OutputStream out = sock.getOutputStream();
        Log.d(TAG, "OutputStream :" + out);

        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        ObdCommand cmd = new ObdResetCommand();
//
//        try {
//            cmd.run(in, out);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        String s = cmd.getResult();
//        Log.d(TAG, s);
//        cmd = new EngineRPMObdCommand();
//        try {
//            cmd.run(in, out);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        s = cmd.getResult();
//        Log.d(TAG, s);
        //       queueJob(new ObdCommandJob(new ObdResetCommand()));
        //     queueJob(new ObdCommandJob(new EchoOffObdCommand()));

    /*
     * Will send second-time based on tests.
     *
     * TODO this can be done w/o having to queue jobs by just issuing
     * command.run(), command.getResult() and validate the result.
     */


        queueJob(new ObdCommandJob(new EchoOffObdCommand()));
        queueJob(new ObdCommandJob(new LineFeedOffObdCommand()));
        queueJob(new ObdCommandJob(new TimeoutObdCommand(62)));
        queueJob(new ObdCommandJob(new SelectProtocolObdCommand(ObdProtocols.ISO_15765_4_CAN)));
        queueJob(new ObdCommandJob(new OdbRawCommand("ATH1")));
        queueJob(new ObdCommandJob(new OdbRawCommand("ATSH7E0")));
        queueJob(new ObdCommandJob(new OdbRawCommand("ATCAF0")));
        queueJob(new ObdCommandJob(new OdbRawCommand("ATCSM0")));

        // For now set protocol to AUTO
        //    queueJob(new ObdCommandJob(new SelectProtocolObdCommand(ObdProtocols.AUTO))); // works for  my LHS
//        queueJob(new ObdCommandJob        (new SelectProtocolObdCommand(ObdProtocols.AUTO)));

        // Job for returning dummy data
        //queueJob(new ObdCommandJob(new AmbientAirTemperatureObdCommand()));

        queueCounter = 0L;
        Log.d(TAG, "Initialization jobs queued.");

        isRunning = true;
    }

    /**
     * Runs the queue until the service is stopped
     */
    synchronized protected void executeQueue() {
        Log.d(TAG, "Executing queue..");
        synchronized (isQueueRunning) {
            isQueueRunning = true;
            while (!jobsQueue.isEmpty()) {
                ObdCommandJob job = null;
                try {
                    job = jobsQueue.take();

                    // log job
                    Log.d(TAG, "Taking job[" + job.getId() + "] from queue..");

                    if (job.getState().equals(ObdCommandJobState.NEW)) {
                        Log.d(TAG, "Job state is NEW. Run it..");
                        job.setState(ObdCommandJobState.RUNNING);
                        int cnt = sock.getInputStream().available();
                        // Cleanup input stream.  (read old data)
                        while (sock.getInputStream().available() != 0) {
                            sock.getInputStream().read();
                        }
                        Log.d(TAG, "available1:" + cnt);
                        String cmd = null;
                        ObdCommand ObdCmd = job.getCommand();
                        cmd = ObdCmd.getCmd();//gets cmd string
                        if (!cmd.endsWith("\r")) {
                            cmd += "\r";
                        }
                        Logger.d("Sent:" + cmd + "\r");
                        // write to OutputStream (i.e.: a BluetoothSocket)
                        sock.getOutputStream().write(cmd.getBytes());
                        sock.getOutputStream().flush();
                        Thread.sleep(50);
                        //Read response
                        StringBuilder res = new StringBuilder();
                        byte b = 0;
                        int cnt1=0;
                        int cnt2=0;
                        // read until '>' arrives or until too long
                        if (cmd.startsWith("ATMT")){//Process normal communication
                            Log.d(TAG, "Job mt. starts ");
                            Thread.sleep(50);
                            while (sock.getInputStream().available() != 0){
                                b = (byte) sock.getInputStream().read();
                                cnt2++;
                                Log.d(TAG, "Job mt. new char = "+b+" cnt2 = " + cnt2);
                                if ((char) b != ' ')//remove spaces
                                    res.append((char) b);
                                if (res.length() > 1 && b == 13) { // Got one complete line
                                    cnt1++;
                                    if (res.toString().startsWith(Integer.toString(job.get_canID()))||
                                            cnt1 == 1){
                                        Log.d(TAG, "Job mt. Line rcv ="+Integer.toString(cnt1));
                                        sock.getOutputStream().write(13);
                                        sock.getOutputStream().flush();
                                        while (sock.getInputStream().available() != 0) {
                                            sock.getInputStream().read();
                                        }
                                        cnt1 = 0;
                                        break;
                                    }
                                }
                            }
                            sock.getOutputStream().write(13);
                            sock.getOutputStream().flush();
                            while (sock.getInputStream().available() != 0) {
                                sock.getInputStream().read();
                            }
                        }
                        else{//Process PID
                            while ((char) (b = (byte) sock.getInputStream().read()) != '>') {
                                if ((char) b != ' ')//remove spaces
                                    res.append((char) b);
                                if (res.length() > 1 && b == 13) { // break at new line or 3+8
                                    break;
                                }

                            }
                        }
                        // 1st line
                        //job.getCommand().getResult();
                        job.getCommand().setrawData(res.toString().trim());
                        cnt = sock.getInputStream().available();
                        Log.d(TAG, "available2:" + cnt);
                        if (job != null) {
                            Log.d(TAG, "Job != null");
                            final ObdCommandJob job2 = job;
                            ((Activity) ctx).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((ObdProgressListener) ctx).stateUpdate(job2);
                                }
                            });
                        }

                       /*while (job.getCommand().isMoreData()){
                            // post current and read more
                            Log.d(TAG, "MoreData");
                            job.getCommand().runMT(sock.getInputStream());
                            // more lines
                            job.getCommand().getResult();

                            final ObdCommandJob job2 = job;
                            ((Activity) ctx).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((ObdProgressListener) ctx).stateUpdateMT(job2.getCommand().getResult());
                                }
                            });
                            // stop if there is next command
                            if(!jobsQueue.isEmpty()){
                                break;
                            }
                        }*/

                    } else
                        // log not new job
                        Log.e(TAG,
                                "Job state was not new, so it shouldn't be in queue. BUG ALERT!");
                } catch (Exception e) {
                    job.setState(ObdCommandJobState.EXECUTION_ERROR);
                    Log.e(TAG, "Failed to run command. -> " + e.getMessage());
                    if (job != null) {
                        final ObdCommandJob job2 = job;
                        ((Activity) ctx).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((ObdProgressListener) ctx).stateUpdate(job2);
                            }
                        });
                    }
                }

            }
            // will run next time a job is queued
            isQueueRunning = false;
        }
    }

    /**
     * Stop OBD connection and queue processing.
     */
    public void stopService() {
        Log.d(TAG, "Stopping service..");

        notificationManager.cancel(NOTIFICATION_ID);
        jobsQueue.removeAll(jobsQueue); // TODO is this safe?
        isRunning = false;

        if (sock != null)
            // close socket
            try {
                sock.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }

        // kill service
        stopSelf();
    }

    public boolean isRunning() {
        return isRunning;
    }

    public class ObdGatewayServiceBinder extends Binder {
        public ObdGatewayService getService() {
            return ObdGatewayService.this;
        }
    }

}