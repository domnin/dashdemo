package pt.lighthouselabs.obd.reader.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import pt.lighthouselabs.obd.commands.ObdCommand;
import pt.lighthouselabs.obd.commands.SpeedObdCommand;
import pt.lighthouselabs.obd.commands.engine.EngineRPMObdCommand;
import pt.lighthouselabs.obd.commands.fuel.FuelConsumptionRateObdCommand;
import pt.lighthouselabs.obd.commands.fuel.FuelEconomyObdCommand;
import pt.lighthouselabs.obd.commands.fuel.FuelLevelObdCommand;
import pt.lighthouselabs.obd.commands.pressure.IntakeManifoldPressureObdCommand;
import pt.lighthouselabs.obd.commands.pressure.PressureObdCommand;
import pt.lighthouselabs.obd.commands.protocol.SelectProtocolObdCommand;
import pt.lighthouselabs.obd.commands.temperature.AirIntakeTemperatureObdCommand;
import pt.lighthouselabs.obd.commands.temperature.AmbientAirTemperatureObdCommand;
import pt.lighthouselabs.obd.commands.temperature.EngineCoolantTemperatureObdCommand;
import pt.lighthouselabs.obd.commands.temperature.TemperatureObdCommand;
import pt.lighthouselabs.obd.enums.AvailableCommandNames;
import pt.lighthouselabs.obd.enums.ObdProtocols;
import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.R;
import pt.lighthouselabs.obd.reader.config.ObdATICommand;
import pt.lighthouselabs.obd.reader.io.AbstractGatewayService;
import pt.lighthouselabs.obd.reader.io.MockObdGatewayService;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob;
import pt.lighthouselabs.obd.reader.io.ObdGatewayService;
import pt.lighthouselabs.obd.reader.net.ObdReading;
import pt.lighthouselabs.obd.reader.net.ObdService;
import pt.lighthouselabs.obd.reader.util.Logger;
import retrofit.RestAdapter;
import retrofit.client.Response;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.main)
public class MainActivity extends RoboActivity implements ObdProgressListener {

    // TODO make this configurable
    private static final boolean UPLOAD = false;
    private Context gaugeCtx = null;

    public static final String TAG = MainActivity.class.getName();
    private static final int NO_BLUETOOTH_ID = 0;
    private static final int BLUETOOTH_DISABLED = 1;
    private static final int START_LIVE_DATA = 2;
    private static final int STOP_LIVE_DATA = 3;
    private static final int SETTINGS = 4;
    private static final int GET_DTC = 5;
    private static final int TABLE_ROW_MARGIN = 7;
    private static final int NO_ORIENTATION_SENSOR = 8;
    private static final int TEST_SCREEN = 9;

    private final SensorEventListener orientListener = new SensorEventListener() {
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            String dir = "";
            if (x >= 337.5 || x < 22.5) {
                dir = "N";
            } else if (x >= 22.5 && x < 67.5) {
                dir = "NE";
            } else if (x >= 67.5 && x < 112.5) {
                dir = "E";
            } else if (x >= 112.5 && x < 157.5) {
                dir = "SE";
            } else if (x >= 157.5 && x < 202.5) {
                dir = "S";
            } else if (x >= 202.5 && x < 247.5) {
                dir = "SW";
            } else if (x >= 247.5 && x < 292.5) {
                dir = "W";
            } else if (x >= 292.5 && x < 337.5) {
                dir = "NW";
            }
            compass.setTextColor(Color.WHITE);
            updateTextView(compass, dir);
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // do nothing
        }
    };
    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning()) {
                queueCommands();
            }
            // run again in 2s
            new Handler().postDelayed(mQueueCommands, 500);
        }
    };
    @InjectView(R.id.compass_text)
    private TextView compass;
    @InjectView(R.id.rpm_text)
    private TextView tvRpm;
    @InjectView(R.id.spd_text)
    private TextView tvSpeed;
    @InjectView(R.id.data_table)
    private TableLayout tl;
    @InjectView(R.id.web_content_rpm)
    private WebView gauge;
    @InjectView(R.id.web_content_speed)
    private WebView gauge_speed;

    // main for landscape view
    @InjectView(R.id.web_content_gauges)
    private WebView all_gauges;


    @Inject
    private SensorManager sensorManager;
    @Inject
    private PowerManager powerManager;
    @Inject
    private SharedPreferences prefs;
    private boolean isServiceBound;


    private AbstractGatewayService service;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(MainActivity.this);
            Log.d(TAG, "Starting the live data");
            service.startService();

        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };

    private Sensor orientSensor = null;
    private PowerManager.WakeLock wakeLock = null;
    private boolean preRequisites = true;

    public void updateTextView(final TextView view, final String txt) {
        new Handler().post(new Runnable() {
            public void run() {
                view.setText(txt);
            }
        });
    }

    @Override
    public void stateUpdateMT(String rawResult) {

    }

    float map = 50, frpm = 1000, vss = 20, imap = 1, maf = 1, iat = 300;

    public void stateUpdate(final ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = job.getCommand().getFormattedResult();
        tvRpm.setTextColor(Color.WHITE);
        Logger.d(cmdName + ":" + cmdResult);
        if (AvailableCommandNames.ENGINE_RPM.getValue().equals(cmdName)) {
            addTableRow(cmdName, cmdResult);
            String rpm = job.getCommand().getResult();
            EngineRPMObdCommand rpmcmd = (EngineRPMObdCommand) job.getCommand();
            if (rpm == null) {
                rpm = "0";
            } else {
                frpm = rpmcmd.getRPM();
                Log.d(TAG, "RPM:" + rpm);
                if (cmdResult == null) {
                    cmdResult = "0";
                }
                if (cmdResult.indexOf(' ') > 0) {
                    cmdResult = "" + Integer.parseInt(cmdResult.substring(0, cmdResult.indexOf(' ')));
                }
                rpm = cmdResult;
            }
            gauge.loadUrl("javascript:setRPM(" + rpm + ")");
            all_gauges.loadUrl("javascript:drawRPM(" + rpm + ")");
        } else if (AvailableCommandNames.SPEED.getValue().equals(
                cmdName)) {
            addTableRow(cmdName, cmdResult);
            SpeedObdCommand spd = (SpeedObdCommand) job.getCommand();
            int speed = (int) (spd.getImperialUnit());
            vss = spd.getMetricSpeed();
            gauge_speed.loadUrl("javascript:setSpeed(" + speed + ")");
            all_gauges.loadUrl("javascript:setSpeed(" + speed + ")");
        } else if (AvailableCommandNames.ENGINE_COOLANT_TEMP.getValue().equals(
                cmdName)) {
            addTableRow(cmdName, cmdResult);
            // engine temp in Fahrenheit
            int etemp = (int) ((TemperatureObdCommand) job.getCommand()).getImperialUnit();
            all_gauges.loadUrl("javascript:drawTemp(" + etemp + ")");

        } else if (AvailableCommandNames.FUEL_LEVEL.getValue().equals(
                cmdName)) {
            addTableRow(cmdName, cmdResult);
            int fuel = (int) ((FuelLevelObdCommand) job.getCommand()).getFuelLevel();
            all_gauges.loadUrl("javascript:drawFuel(" + fuel + ")");

        } else if (AvailableCommandNames.AMBIENT_AIR_TEMP.getValue().equals(
                cmdName)) {
            float fT = ((TemperatureObdCommand) job.getCommand()).getImperialUnit();
            addTableRow(cmdName, "" + fT);
            int airTemp = (int) ((TemperatureObdCommand) job.getCommand()).getImperialUnit();
            all_gauges.loadUrl("javascript:setOutsideTemp(" + airTemp + ")");

        } else if (AvailableCommandNames.AIR_INTAKE_TEMP.getValue().equals(
                cmdName)) {
            float kT = ((TemperatureObdCommand) job.getCommand()).getKelvin();
            iat = kT;
            addTableRow("IAT K", "" + kT);
            Logger.d("IAT K:" + kT);

            //calculate mpg
            imap = frpm * map / iat;
            maf = (float) ((imap / 120.) * 0.8 * 3.5 * 28.97 / 8.314);
            double mpg = (14.7 * 6.17 * 454 * vss) / (3600. * (maf / 100.)) / 150.;
            addTableRow("MPG:", "" + (int) mpg);

            Logger.d("MPG:" + (float) mpg);

            all_gauges.loadUrl("javascript:drawMPG(" + (float) mpg + ")");


        } else {
            if (AvailableCommandNames.INTAKE_MANIFOLD_PRESSURE.getValue().equals(
                    cmdName)) {
                float pkpa = ((PressureObdCommand) job.getCommand()).getMetricUnit();
                addTableRow("MAP KPA", "" + pkpa);
                Logger.d("MAP KPA:" + pkpa);
                map = pkpa;

            } else if (AvailableCommandNames.MAF.getValue().equals(cmdName))
                addTableRow("MAF", cmdResult);
            else if (AvailableCommandNames.EQUIV_RATIO.getValue().equals(cmdName))
                addTableRow(cmdName, cmdResult);
            else
                addTableRow(cmdName, cmdResult);
        }


        if (UPLOAD) {
            Map<String, String> commandResult = new HashMap<String, String>();
            commandResult.put(cmdName, cmdResult);
            // TODO get coords from GPS, if enabled, and set VIN properly
            ObdReading reading = new ObdReading(0d, 0d, System.currentTimeMillis(), "UNDEFINED_VIN", commandResult);
            new UploadAsyncTask().execute(reading);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.d("onCreate");
        // initialize your android device sensor capabilities
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        // get Bluetooth device
        final BluetoothAdapter btAdapter = BluetoothAdapter
                .getDefaultAdapter();
        Display display = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        Log.d(TAG, "orientation:" + rotation);
        if (rotation == Surface.ROTATION_0) {
            if (isFullScreen()) {
                setFullScreen(false);
            }
        } else {
            if (!isFullScreen()) {
                setFullScreen(false);
            }
        }

        preRequisites = btAdapter == null ? false : true;
        if (preRequisites)
            preRequisites = btAdapter.isEnabled();

        if (!preRequisites) {
            showDialog(BLUETOOTH_DISABLED);
            Toast.makeText(this, "BT is disabled, will use Mock service instead", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Blutooth is ok", Toast.LENGTH_SHORT).show();
        }
        gauge = (WebView) findViewById(R.id.web_content_rpm);
        gauge_speed = (WebView) findViewById(R.id.web_content_speed);
        if (gauge != null) {
            gauge.getSettings().setJavaScriptEnabled(true);
            gauge_speed.getSettings().setJavaScriptEnabled(true);
            gauge.loadUrl("file:///android_asset/gaugeMobileRPM.html?(android)&=fullscreen");
            gauge_speed.loadUrl("file:///android_asset/gaugeMobileSpeed.html?(android)&=fullscreen");
            all_gauges.getSettings().setJavaScriptEnabled(true);
//        all_gauges.loadUrl("file:///android_asset/landscape/index.html");

            all_gauges.loadDataWithBaseURL("file:///android_asset/landscape/", readFile("landscape/index.html"), "text/html", "UTF-8", "");
//        all_gauges.getSettings().setBuiltInZoomControls(true);


            all_gauges.setInitialScale(97);
        }

        // get Orientation sensor
//        orientSensor = sensorManager.getSensorList(Sensor.TYPE_ORIENTATION).get(0);
//        if (orientSensor == null)
//            showDialog(NO_ORIENTATION_SENSOR);
    }

    String readFile(String name) {
        try {
            Reader r = new InputStreamReader(this.getAssets().open(name), "utf-8");
            StringBuilder sb = new StringBuilder();
            int c = 0;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
            // Logger.d(sb.toString());
            return sb.toString();

        } catch (IOException e) {
            return "";
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Entered onStart...");

    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        gauge_speed.loadUrl("javascript:setSpeed(" + "70" + ")");
//        gauge.loadUrl("javascript:setRPM(1000)");
//        return super.onTouchEvent(event);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.d("onDestroy");
        releaseWakeLockIfHeld();
        if (isServiceBound) {
            doUnbindService();
        }
        Logger.closeLog();
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Pausing..");
        releaseWakeLockIfHeld();
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
        if (wakeLock.isHeld())
            wakeLock.release();
    }

    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resuming..");
        sensorManager.registerListener(orientListener, orientSensor,
                SensorManager.SENSOR_DELAY_UI);
        // for the system's orientation sensor registered listeners


        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "ObdReader");

    }


    private void updateConfig() {
        startActivity(new Intent(this, ConfigActivity.class));
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, START_LIVE_DATA, 0, "Start Live Data");
        menu.add(0, STOP_LIVE_DATA, 0, "Stop Live Data");
        menu.add(0, GET_DTC, 0, "Get DTC");
        menu.add(0, SETTINGS, 0, "Settings");
        menu.add(0, TEST_SCREEN, 0, "Test Codes");

        return true;
    }

    // private void staticCommand() {
    // Intent commandIntent = new Intent(this, ObdReaderCommandActivity.class);
    // startActivity(commandIntent);
    // }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case START_LIVE_DATA:
                startLiveData();
                return true;
            case STOP_LIVE_DATA:
                stopLiveData();
                return true;
            case SETTINGS:
                updateConfig();
                return true;
            case GET_DTC:
                getTroubleCodes();
                return true;
            case TEST_SCREEN:
                startActivity(new Intent(this, TestScreenActivity.class));
                return true;
            // case COMMAND_ACTIVITY:
            // staticCommand();
            // return true;
        }
        return false;
    }

    private void getTroubleCodes() {
        startActivity(new Intent(this, TroubleCodesActivity.class));
    }

    private void startLiveData() {
        Log.d(TAG, "Starting live data..");

        doBindService();

        // start command execution
        new Handler().post(mQueueCommands);

        // screen won't turn off until wakeLock.release()
        wakeLock.acquire();
    }

    private void stopLiveData() {
        Log.d(TAG, "Stopping live data..");

        doUnbindService();

        releaseWakeLockIfHeld();
    }

    protected Dialog onCreateDialog(int id) {
        AlertDialog.Builder build = new AlertDialog.Builder(this);
        switch (id) {
            case NO_BLUETOOTH_ID:
                build.setMessage("Sorry, your device doesn't support Bluetooth.");
                return build.create();
            case BLUETOOTH_DISABLED:
                build.setMessage("You have Bluetooth disabled. Please enable it!");
                return build.create();
            case NO_ORIENTATION_SENSOR:
                build.setMessage("Orientation sensor missing?");
                return build.create();
        }
        return null;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem startItem = menu.findItem(START_LIVE_DATA);
        MenuItem stopItem = menu.findItem(STOP_LIVE_DATA);
        MenuItem settingsItem = menu.findItem(SETTINGS);
        MenuItem getDTCItem = menu.findItem(GET_DTC);
        MenuItem getTSItem = menu.findItem(TEST_SCREEN);

        if (service != null && service.isRunning()) {
            getDTCItem.setEnabled(false);
            startItem.setEnabled(false);
            getDTCItem.setEnabled(false);
            stopItem.setEnabled(true);
            settingsItem.setEnabled(false);
        } else {
            getDTCItem.setEnabled(true);
            getDTCItem.setEnabled(true);
            stopItem.setEnabled(false);
            startItem.setEnabled(true);
            settingsItem.setEnabled(true);
        }

        return true;
    }

    @Override
    public void addTableRow(String key, String val) {
        TableRow tr = new TableRow(this);
        MarginLayoutParams params = new ViewGroup.MarginLayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(TABLE_ROW_MARGIN, TABLE_ROW_MARGIN, TABLE_ROW_MARGIN,
                TABLE_ROW_MARGIN);
        tr.setLayoutParams(params);
        tr.setBackgroundColor(Color.BLACK);

        TextView name = new TextView(this);
        name.setGravity(Gravity.RIGHT);
        name.setText(key + ": ");
        name.setTextColor(Color.WHITE);
        TextView value = new TextView(this);
        value.setGravity(Gravity.LEFT);
        value.setTextColor(Color.WHITE);

        value.setText(val);
        tr.addView(name);
        tr.addView(value);
        tl.addView(tr, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));

		/*
     * TODO remove this hack
		 *
		 * let's define a limit number of rows
		 */
        if (tl.getChildCount() > 18)
            tl.removeViewAt(0);
    }

    static int modCnt = 0;

    /**
     *
     */
    synchronized private void queueCommands() {
        if (isServiceBound) {

//      final ObdCommandJob airTemp = new ObdCommandJob(
//          new AmbientAirTemperatureObdCommand());
            // VSS
            final ObdCommandJob speed = new ObdCommandJob(new SpeedObdCommand());
            service.queueJob(speed);
//      final ObdCommandJob fuelEcon = new ObdCommandJob(
//          new FuelEconomyObdCommand());
            // RPM
            final ObdCommandJob rpm = new ObdCommandJob(new EngineRPMObdCommand());
            service.queueJob(rpm);


//        service.queueJob(airTemp);
//      service.queueJob(fuelEcon);
//            final ObdCommandJob ati = new ObdCommandJob(new ObdATICommand(""));
//            service.queueJob(ati);
            if ((++modCnt) % 2 == 0) {
                final ObdCommandJob fuelLevel = new ObdCommandJob(
                        new FuelLevelObdCommand());
                service.queueJob(fuelLevel);
                final ObdCommandJob coolant = new ObdCommandJob(
                        new EngineCoolantTemperatureObdCommand());
                service.queueJob(coolant);
                final ObdCommandJob fuelRate = new ObdCommandJob(
                        new FuelConsumptionRateObdCommand());
                service.queueJob(fuelRate);
                final ObdCommandJob airTemp = new ObdCommandJob(
                        new AmbientAirTemperatureObdCommand());
                service.queueJob(airTemp);
                // IAT
                final ObdCommandJob intakeAirTemp = new ObdCommandJob(
                        new AirIntakeTemperatureObdCommand());
                service.queueJob(intakeAirTemp);
                // MAP
                final ObdCommandJob manifoldAirPressure = new ObdCommandJob(
                        new IntakeManifoldPressureObdCommand());
                service.queueJob(manifoldAirPressure);


            }


        }
    }

    private void doBindService() {
        if (!isServiceBound) {
            Log.d(TAG, "Binding OBD service..");
            if (preRequisites) {
                Intent serviceIntent = new Intent(this, ObdGatewayService.class);
                bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
            } else {
                Intent serviceIntent = new Intent(this, MockObdGatewayService.class);
                bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);
            }
        }
    }

    private void doUnbindService() {
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }

    /**
     * Uploading asynchronous task
     */
    private class UploadAsyncTask extends AsyncTask<ObdReading, Void, Void> {

        @Override
        protected Void doInBackground(ObdReading... readings) {
            Log.d(TAG, "Uploading " + readings.length + " readings..");
            // instantiate reading service client
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://server_ip:8080/obd")
                    .build();
            ObdService service = restAdapter.create(ObdService.class);
            // upload readings
            for (ObdReading reading : readings) {
                Response response = service.uploadReading(reading);
                assert response.getStatus() == 200;
            }
            Log.d(TAG, "Done");
            return null;
        }

    }


    public boolean isFullScreen() {

        return (getWindow().getAttributes().flags &
                WindowManager.LayoutParams.FLAG_FULLSCREEN) != 0;
    }

    @SuppressLint("NewApi")
    public void setFullScreen(boolean full) {

        if (full == isFullScreen()) {
            return;
        }

        Window window = getWindow();
        if (full) {
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        if (Build.VERSION.SDK_INT >= 11) {
            if (full) {
                getActionBar().hide();
            } else {
                getActionBar().show();
            }
        }
    }

    static boolean scrToggle = false;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        setFullScreen(scrToggle);
        scrToggle = !scrToggle;
        Log.d(TAG, "scrToggle:" + scrToggle);
        String rpm = " " + new Random().nextInt(1000);
//        all_gauges.loadUrl("javascript:drawRPM(" + rpm + ")");
//        Log.d(TAG,"rpm:"+rpm);
//        int speed = new Random().nextInt(200);
//        all_gauges.loadUrl("javascript:setSpeed(" + speed + ")");
//        int etemp = new Random().nextInt(260);
//        all_gauges.loadUrl("javascript:drawTemp(" + etemp + ")");
//        int gas = new Random().nextInt(100);
//        int gas=33;
//        all_gauges.loadUrl("javascript:drawFuel(" + gas + ")");
//        int mpg = new Random().nextInt(36);
//        all_gauges.loadUrl("javascript:drawMPG(" + mpg + ")");
//        int temp = new Random().nextInt(90);
//        all_gauges.loadUrl("javascript:setOutsideTemp(" + temp + ")");
//        all_gauges.loadUrl("javascript:drawMPG(22)");

        return super.onTouchEvent(event);
    }
}