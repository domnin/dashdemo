package pt.lighthouselabs.obd.commands;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileReader;//dyd
import java.io.LineNumberReader;//dyd
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by KsmUser on 2/2/2015.
 */
public class Parameters {
    public static final String TAG = "Parameters";
    public class Param {
        private int _paramID;
        private String _paramName;
        private String _atCmd;
        private BigInteger _mask;
        private int _shift;
        private double _scale;
        private String _engUnits;
        private String _engParamValue;

        public Param() {
            this.set_paramID("0");
            this.set_paramName("0");
            this.set_atCmd("0");
            this.set_mask("0");
            this.set_shift("0");
            this.set_scale("0");
            this.set_engUnits("0");
            this.set_engParamValue("");
        }
        public int get_paramID(){
            return _paramID;
        }
        public String get_paramName(){
            return _paramName;
        }
        public String get_atCmd(){
            return _atCmd;
        }
        public BigInteger get_mask(){
            return _mask;
        }
        public int get_shift(){
            return _shift;
        }
        public double get_scale(){
            return _scale;
        }
        public String get_engUnits(){
            return _engUnits;
        }
        public String get_engParamValue(){ return _engParamValue;}
        public void set_paramID(String paramID) {
            this._paramID = Integer.parseInt(paramID,16);
        }
        public void set_paramName(String paramName) {
            this._paramName = paramName;
        }
        public void set_atCmd(String atCmd) {
            this._atCmd = atCmd;
        }
        public void set_mask(String mask) {
            this._mask = new BigInteger(mask, 16);
        }
        public void set_shift(String shift) {
            this._shift = Integer.parseInt(shift);
        }
        public void set_scale(String scale) {
            this._scale = Double.parseDouble(scale);
        }
        public void set_engUnits(String engUnits) {
            this._engUnits = engUnits;
        }
        public void set_engParamValue(String engParamValue){
            this._engParamValue = engParamValue;
        }
    }/* End of Param class */
    ArrayList<Param> paramPIDs = new ArrayList<Param>();
    public ArrayList<Param> getPidParamList() {
        return paramPIDs;
    }
    ArrayList<Param> paramNcomm = new ArrayList<Param>();
    public ArrayList<Param> getNcommParamList () {
        return paramNcomm;
    }
    public Param getPidParam(int paramIndx) {
        return paramPIDs.get(paramIndx);
    }
    public Param getNcommParam(int paramIndx) {
        return paramNcomm.get(paramIndx);
    }
    public void populatePidParametersFromFile() {
        File root = Environment.getExternalStorageDirectory();
        try {
            LineNumberReader lr = new LineNumberReader(new FileReader(new File(root, "ConfigPid.txt")));

            if (lr != null) {
                String receiveString = "";
                int i=0;
                while ((receiveString = lr.readLine()) != null) {
                    Param param = new Param();
                    param.set_paramID(receiveString);
                    param.set_paramName(lr.readLine());
                    param.set_atCmd(lr.readLine());
                    param.set_mask(lr.readLine());
                    param.set_shift(lr.readLine());
                    param.set_scale(lr.readLine());
                    param.set_engUnits(lr.readLine());
                    paramPIDs.add(param);
                    i++;
                }

                lr.close();
               // ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.d(TAG,"File not found: " + e.toString());
        } catch (IOException e) {
            Log.d(TAG, "Can not read file: " + e.toString());
        }
    }/* End of populatePidParametersFromFile() */
    public void populateNcommParametersFromFile() {
        File root = Environment.getExternalStorageDirectory();
        try {
            LineNumberReader lrNcomm = new LineNumberReader(new FileReader(new File(root, "ConfigNcomm.txt")));

            if (lrNcomm != null) {
                String receiveString = "";
                int i=0;
                while ((receiveString = lrNcomm.readLine()) != null) {
                    Param paramNcomms = new Param();
                    paramNcomms.set_paramID(receiveString);
                    paramNcomms.set_paramName(lrNcomm.readLine());
                    paramNcomms.set_atCmd(lrNcomm.readLine());
                    paramNcomms.set_mask(lrNcomm.readLine());
                    paramNcomms.set_shift(lrNcomm.readLine());
                    paramNcomms.set_scale(lrNcomm.readLine());
                    paramNcomms.set_engUnits(lrNcomm.readLine());
                    paramNcomm.add(paramNcomms);
                    i++;
                }

                lrNcomm.close();
                // ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.d(TAG,"File not found: " + e.toString());
        } catch (IOException e) {
            Log.d(TAG, "Can not read file: " + e.toString());
        }
    }/* End of populateNcommParametersFromFile() */
    public void parsePidParValue(String obdRespString, int paramIndex){
        String data = obdRespString;
        StringBuilder dataSB = new StringBuilder(obdRespString+"00000000000");
        data = dataSB.substring(0, 6).toString();//Trim data string to 8 symbols
        Log.d(TAG, "parsePidParValue Trimmed " + data);
        if (data.matches("([0-9A-F]{2})+")){
            BigInteger biData = new BigInteger(data, 16);
            Log.d(TAG, "parsePidParValue data BigInt" + biData.toString());
            Log.d(TAG, "parsePidParValue mask BigInt" + this.paramPIDs.get(paramIndex)._mask.toString());
            biData = biData.and(this.paramPIDs.get(paramIndex)._mask).shiftRight(this.paramPIDs.get(paramIndex)._shift);
            Log.d(TAG, "parsePidParValue raw Data BigInt" + biData.toString());
            this.paramPIDs.get(paramIndex).set_engParamValue((biData.toString()));
            Log.d(TAG, "parsePidParValue engParamValue" + this.paramPIDs.get(paramIndex)._engParamValue +
                this.paramPIDs.get(paramIndex)._engUnits);
        }
    }
    public void parseNcommParValue(String obdRespString, int paramIndex){
        String data = obdRespString;
        StringBuilder dataSB = new StringBuilder(obdRespString+"00000000000");
        data = dataSB.substring(3, 11).toString();//Trim data string to 8 symbols
        Log.d(TAG, "parseNcommParValue Trimmed " + data);
        if (data.matches("([0-9A-F]{2})+")){
            BigInteger biData = new BigInteger(data, 16);
            Log.d(TAG, "parseNcommParValue data BigInt" + biData.toString());
            Log.d(TAG, "parseNcommParValue mask BigInt" + this.paramNcomm.get(paramIndex)._mask.toString());
            biData = biData.and(this.paramNcomm.get(paramIndex)._mask).shiftRight(this.paramNcomm.get(paramIndex)._shift);
            Log.d(TAG, "parseNcommParValue raw Data BigInt" + biData.toString());
            this.paramNcomm.get(paramIndex).set_engParamValue((biData.toString()));
            Log.d(TAG, "parseNcommParValue engParamValue" + this.paramNcomm.get(paramIndex)._engParamValue +
                    this.paramNcomm.get(paramIndex)._engUnits);
        }
    }
}
