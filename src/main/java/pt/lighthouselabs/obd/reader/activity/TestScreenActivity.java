package pt.lighthouselabs.obd.reader.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.inject.Inject;

import org.mvel2.MVEL;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import pt.lighthouselabs.obd.commands.ObdCommand;
import pt.lighthouselabs.obd.commands.Parameters;
import pt.lighthouselabs.obd.commands.protocol.OdbRawCommand;
import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.R;
import pt.lighthouselabs.obd.reader.io.AbstractGatewayService;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob;
import pt.lighthouselabs.obd.reader.io.ObdGatewayService;
import pt.lighthouselabs.obd.reader.util.Logger;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

public class TestScreenActivity extends RoboActivity implements ObdProgressListener {
    public static final String TAG = TestScreenActivity.class.getName();
    private static final int TABLE_ROW_MARGIN = 7;
    private static ArrayList<String> scrollData = new ArrayList<String>();
    private static String formulaString;
    private static Parameters paramList = new Parameters();
    @InjectView(R.id.test_data_view)
    private TextView ttv;
    @InjectView(R.id.test_data_scroll)
    private ScrollView scroll;
    @Inject
    private PowerManager powerManager;
   // @InjectView(R.id.command_text)
   // private TextView commandTextView;
    @InjectView(R.id.formula_value)
    private TextView formulaValue;
    @InjectView(R.id.button_start_stop)
    private Button startStopButton;
    private ObdCommand obdCommand;
    @InjectView(R.id.button_connect_disconnect)
    private Button connectButton;
    @InjectView(R.id.button_send)
    private Button sendButton;

    @InjectView(R.id.test_layout)
    private LinearLayout testScreenLayout;


    @Inject
    private SharedPreferences prefs;
    private boolean isServiceBound;
    private AbstractGatewayService service;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(TestScreenActivity.this);
            Log.d(TAG, "Starting the live data");
            service.startService();

        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };
    /*
    Rune one time
     */
    private final Runnable mSendCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning()) {
                queueCommands();
            }
        }
    };
    private PowerManager.WakeLock wakeLock = null;
    private boolean isLoop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActionBar().hide();
        paramList.populatePidParametersFromFile();
        paramList.populateNcommParametersFromFile();

        connectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!isServiceBound) {
                    Logger.d("Connect...");
//                    startLiveData();
                    doBindService();
                    Logger.d("Connected");
                    // screen won't turn off until wakeLock.release()
                    wakeLock.acquire();
                    connectButton.setText("Disconnect");
                } else {
                    Logger.d("Disconnect....");
//                    stopLiveData();
                    doUnbindService();
                    releaseWakeLockIfHeld();
                    connectButton.setText("Connect");
                    Logger.d("Disconnected");
                }
            }
        });

        connectButton.setText(isServiceBound ? "Disconnect" : "Connect");


        // send one time
        sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // CharSequence s = commandTextView.getText();
              //  formulaString = formulaTextView.getText() + "";
                obdCommand = new OdbRawCommand("" + "010c");
                Logger.d("Start: " + "010c");
                // start command execution`
                new Handler().post(mSendCommands);
            }
        });
        startStopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (isLoop == false) {
                    /*CharSequence s = commandTextView.getText();
                    formulaString = formulaTextView.getText() + "";
                    obdCommand = new OdbRawCommand("" + s);*/
                    isLoop = true;
                    // read parameter list from file and start command execution
                    //paramList.populatePidParametersFromFile();
                    new Handler().post(mQueueCommands);
                    Logger.d("Connected");
                    // screen won't turn off until wakeLock.release()
                    wakeLock.acquire();
                    startStopButton.setText("Stop");
                } else {
                    Logger.d("Disconnect....");
                    isLoop = false;
                    startStopButton.setText("START");
                    Logger.d("Disconnected");
                }
            }
        });

        if (scrollData.size() == 0) {
            for (int i = 0; i < 120; i++) {
                addTableRow("", "");
            }
        } else {
            for (String s : scrollData) {

                addRow(s);
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_test_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void stateUpdateMT(String cmdResult) {
        String[] resArray;
        if (cmdResult.contains("\r")) {
            resArray = cmdResult.split("\r");
        } else {
            resArray = new String[]{cmdResult};
        }
        try {
            for (String s : resArray) {
                cmdResult = s;
                addTableRow("", cmdResult);
                formulaValue.setText(paramList.getPidParam(1).get_engParamValue()+paramList.getPidParam(1).get_engUnits());
            }
        } catch (Throwable e) {
            if (e != null) {
                addTableRow("Error", e.getMessage());
            } else {
                addTableRow("Error", "null");

            }
        }
    }

    void calculateByteResult(String cmdResult) {
        cmdResult = cmdResult.replace(" ", "");
        String[] arr = formulaString.split(":");
        String pref = arr[0];
        String expr = arr[1];
        if (pref != null && expr != null && expr.length() != 0 && cmdResult.startsWith(pref)) {
            String data = cmdResult.substring(pref.length());
            Integer[] bytes = new Integer[data.length() / 2];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = new BigInteger(data.substring(i * 2, (i + 1) * 2), 16).intValue();
            }
            Map<String, Object> mvelVars = new HashMap<String, Object>();
            mvelVars.put("B", bytes);
            Object o = MVEL.eval(expr, mvelVars);
            formulaValue.setText(o.toString());
        }

    }

    @Override
    public void stateUpdate(ObdCommandJob job) {
        final String cmdName = job.getCommand().getName();
        String cmdResult = job.getCommand().getResult();
        Log.d(TAG, "stateUpdate Cmd = "+cmdResult+ " Index = "+job.get_paramListIndxId());

        if (job.getCommand().getCmd().startsWith("ATMT")){
            paramList.parseNcommParValue(cmdResult, job.get_paramListIndxId());
            addTableRow("Ncomm ", paramList.getNcommParam(job.get_paramListIndxId()).get_paramName() + " = "
                    + paramList.getNcommParam(job.get_paramListIndxId()).get_engParamValue() + " "
                    + paramList.getNcommParam(job.get_paramListIndxId()).get_engUnits());
            Log.d(TAG, "stateUpdate addTableRow" + paramList.getNcommParam(job.get_paramListIndxId()).get_paramName());
        }
        else {
            paramList.parsePidParValue(cmdResult, job.get_paramListIndxId());
            addTableRow("Obd ", paramList.getPidParam(job.get_paramListIndxId()).get_paramName() + " = "
                    + paramList.getPidParam(job.get_paramListIndxId()).get_engParamValue() + " "
                    + paramList.getPidParam(job.get_paramListIndxId()).get_engUnits());
            Log.d(TAG, "stateUpdate addTableRow" + paramList.getPidParam(job.get_paramListIndxId()).get_paramName());
        }
        addTableRow("Raw Data: ", cmdResult);
        formulaValue.setText(paramList.getPidParam(1).get_engParamValue()+paramList.getPidParam(1).get_engUnits());
    }

    @Override
    public void addTableRow(String key, String val) {
        String s = key + ":" + val + "\r\n";
        addRow(s);
        scrollData.add(s);
        if (scrollData.size() > 100) {
            scrollData.remove(0);
        }
    }

    private void addRow(String s) {
        ttv.append(s);
        scroll.post(new Runnable() {
            @Override
            public void run() {
                scroll.fullScroll(View.FOCUS_DOWN);
            }
        });

    }

    protected void doBindService() {
        if (!isServiceBound) {
            Log.d(TAG, "Binding OBD service..");

            Intent serviceIntent = new Intent(this, ObdGatewayService.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);

        }
    }

    protected void doUnbindService() {
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }
    //Parameters paramList = new Parameters();
   // paramList.populatePidParametersFromFile();
    synchronized protected void queueCommands() {
        if (isServiceBound) {
            for (int i = 0; i < paramList.getPidParamList().size(); i++) {
                obdCommand = new OdbRawCommand(paramList.getPidParam(i).get_atCmd());
                Logger.d("Start: " + paramList.getPidParam(i).get_atCmd());
                final ObdCommandJob job = new ObdCommandJob(obdCommand);
                job.set_paramListIndx(i);
                service.queueJob(job);
            }
            for (int i = 0; i < paramList.getNcommParamList().size(); i++) {
                obdCommand = new OdbRawCommand(paramList.getNcommParam(i).get_atCmd());
                Logger.d("Start: " + paramList.getNcommParam(i).get_atCmd());
                final ObdCommandJob job = new ObdCommandJob(obdCommand);
                job.set_paramListIndx(i);
                job.set_canID(paramList.getNcommParam(i).get_paramID());
                service.queueJob(job);
            }
        }
    }/*End qeueCommands */

    private void startLiveData() {
        Log.d(TAG, "Starting live data..");

        doBindService();

        // start command execution
        new Handler().post(mQueueCommands);

        // screen won't turn off until wakeLock.release()
        wakeLock.acquire();
    }

    private final Runnable mQueueCommands = new Runnable() {
        public void run() {
            if (service != null && service.isRunning() && isLoop) {
                queueCommands();
                // run again after delay
                new Handler().postDelayed(mQueueCommands, 1500); // 1500 ms in loop
            }
        }
    };

    private void stopLiveData() {
        Log.d(TAG, "Stopping live data..");

        doUnbindService();

        releaseWakeLockIfHeld();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Pausing..");
        releaseWakeLockIfHeld();
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
        if (wakeLock.isHeld())
            wakeLock.release();
    }

    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resuming..");

        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "ObdReader");

    }


}

