// JavaScript Document


//RPM
var realRPMValue;//Ranges from 0 - 8000;
//FUEL
var fuelPercentage;//Ranges from 0 - 100;
//MPG
var mpgValue;//Ranges from 0 - 36; (this will need to be adjusted on the car specs)
//TEMP
var engineTemperature;//Ranges from 100 - 260;(this will need to be adjusted on the car specs)
//avgMPG
var avgMpgValue;//Ranges from 0 - 36; (this will need to be adjusted on the car specs)
//trip
var tripValue;
//total miles
var milesTotal;
//speed
var speedValue;
//outside temperature
var outsideTempValue;

var firstTime;




//variables needed for the animations
var animaDuration = 200;
var animaFuel = 0;
var animaRPM = 0;
var animaTemp = 100;
var animaMPG = 0;
var fuelAnimation_loop, rpmAnimation_loop, tempAnimation_loop, mpgAnimation_loop, intro_anima;



function setSpeed(speed){
	speedValue = speed;
	document.getElementById("speedDisplay").innerHTML = speedValue;								
}

function setAvgMPG(avgmpg){
	avgMpgValue = avgmpg;
	document.getElementById("avgMpgDisplay").innerHTML = avgMpgValue+" mi";						
}
function setTrip(trip){
	tripValue = trip;
	document.getElementById("tripDisplay").innerHTML = tripValue;																
}
function setTotalMiles(miles){
	milesTotal = miles;
	document.getElementById("totalMilesDisplay").innerHTML = milesTotal+" miles";														
}
function setOutsideTemp(outsideTemp){
	outsideTempValue = outsideTemp;
	document.getElementById("outsideTempDisplay").innerHTML = outsideTempValue + "&deg; F";
}


window.onload= function(){
	//RPM_start();
	
	startPage();

};

function startPage(){
	//Set max values for "testing" when engine starts.
		drawFuel(100);
		drawTemp(260);
		drawMPG(36);
		setSpeed(230);
		setAvgMPG(36.0);
		setTrip(999.9);
		setTotalMiles(9999999);
		setOutsideTemp(999);
		$(document.getElementById("leftPage")).fadeIn(1000);
		$(document.getElementById("rightPage")).fadeIn(1000);
		$(document.getElementById("welcome")).fadeIn(1000).delay(2000,null).fadeOut(1000, null, startGauges);
		
}
function startGauges(){
		firstTime = 1;
		$(document.getElementById("centerGauge")).fadeIn(1000);
		$(document.getElementById("odometer")).fadeIn(1000);
		//here to set the real values
		drawRPM(7495);
		drawFuel(42);	
		drawTemp(150);
		drawMPG(23.6);
		setSpeed(132);
		setAvgMPG(23.1);
		setTrip(429.1);
		setTotalMiles(321291);
		setOutsideTemp(65);
			
}



function initFuel(value){
	//FUEL
	//calculate fuel percentage translated to the bar
	//var fuelAngle = ((fuelPercentage*48)/100)+151;
	//document.getElementById("fuelText").innerHTML ="animaFuel = " +animaFuel;	
	fuelAngle = ((animaFuel*48)/100)+151;
	var canvasFuel = document.getElementById('myCanvasFuel');
	var contextFuel = canvasFuel.getContext('2d');
	var xFuel = canvasFuel.width / 2+105;
	var yFuel = canvasFuel.height / 2-12;
	var radius = 375;
	var startAngle =  151 * Math.PI/180;
	var endAngle = fuelAngle * Math.PI/180;
	//var endAngle = 199 * Math.PI/180;
	var counterClockwise = false;
	contextFuel.clearRect(0, 0, canvasFuel.width, canvasFuel.height);
	contextFuel.beginPath();
	contextFuel.arc(xFuel, yFuel, radius, startAngle, endAngle, counterClockwise);
	contextFuel.lineWidth = 25;
	
	//document.getElementById("fuelText").innerHTML ="animaFuel = " +animaFuel;	
	//document.getElementById("fuelText2").innerHTML ="value = " +value;	
	
	
	// gauge color
	if(fuelPercentage<15){
		contextFuel.strokeStyle = '#d61010';
		document.getElementById("fuelIcon").className = "red-circle position1";
		document.getElementById("fuelMeter").className = "fltleft col1 meter-left-red";
	}else{
		contextFuel.strokeStyle = '#0fa3d1';
		document.getElementById("fuelIcon").className = "blue-circle position1";
		document.getElementById("fuelMeter").className = "fltleft col1 meter-left";
	}
	
	
	contextFuel.stroke();
}



function drawFuel(fuel){
	if(fuel>0){
	var difference = fuel - fuelPercentage;
	fuelPercentage = fuel;
	
	//Cancel any movement animation if a new chart is requested
	if(typeof fuelAnimation_loop != undefined) clearInterval(fuelAnimation_loop);
	//The animation will take 1 second
	//time for each frame is 1sec / difference in degrees
	fuelAnimation_loop = setInterval(animateFuel_to, animaDuration/difference);
	}else{
		drawFuel(1);
		}
}

function animateFuel_to(){
	//clear animation loop if degrees reaches to new_degrees
	if(animaFuel == fuelPercentage) clearInterval(fuelAnimation_loop);
	
	if(animaFuel < fuelPercentage)
		animaFuel++;
		else
		animaFuel--;
	
	initFuel();
}


//RPM
function initRPM(){
	
	
	
	var rpmAngle = ((animaRPM*145)/8000)+134;
	//var rpmAngle = ((realRPMValue*145)/8000)+134;
	var canvas3 = document.getElementById('myCanvas3');
	var context3 = canvas3.getContext('2d');
	var x = canvas3.width / 2+6;
	var y = canvas3.height / 2+3;
	var radius = 255;
	var startDegrees = 134 * Math.PI/180;
	//var endDegrees = 279 * Math.PI/180;
	var endDegrees = rpmAngle * Math.PI/180;
	var startAngle = startDegrees;
	var endAngle = endDegrees;
	var counterClockwise = false;
	
	
	
	
	context3.clearRect(0, 0, canvas3.width, canvas3.height);
	
	var gradBlue = context3.createLinearGradient(0,0,-100,100);
	gradBlue.addColorStop(0, '#d61010');
	gradBlue.addColorStop(1, '#0fa3d1');
	
	context3.save();
	context3.beginPath();
	context3.rect(0, 0, canvas3.width, canvas3.height);
	context3.clip();
	
	context3.strokeStyle = gradBlue;
	
	context3.beginPath();
	context3.arc(x, y, radius, startAngle, endAngle, counterClockwise);
	context3.lineWidth = 23;
	
	// line color
	//context3.strokeStyle = '#0fa3d1';
	context3.stroke();
	context3.restore();
	
	
	
}


function drawRPM(rpm){
	if (rpm>0){
		//rpm = Math.round(Math.random()*8000);
		var difference = rpm - realRPMValue;
		realRPMValue = rpm;
		//Cancel any movement animation if a new chart is requested
		if(typeof rpmAnimation_loop != undefined) clearInterval(rpmAnimation_loop);
		//The animation will take 1 second
		//time for each frame is 1sec / difference in degrees
		rpmAnimation_loop = setInterval(animateRPM_to,0);
	}else{
		drawRPM(1);
		}
}



function animateRPM_to(){
	//clear animation loop if degrees reaches to new_degrees
	if((animaRPM/100) == realRPMValue/100) 
	clearInterval(rpmAnimation_loop);
	
	if((animaRPM/100) < realRPMValue/100)
	animaRPM+=100;
	else
	animaRPM-=100;		
	initRPM();
}



//Temperature
function initTemp(){
	var tempAngle =29-(((animaTemp-100)*48)/160);
	//var tempAngle =29-(((engineTemperature-100)*48)/160);
	var canvas1 = document.getElementById('myCanvas1');
	var context1 = canvas1.getContext('2d');
	var x = canvas1.width / 2-100;
	var y = canvas1.height / 2-12;
	var radius = 375;
	var startAngle = 29* Math.PI/180;
	var endAngle = tempAngle * Math.PI/180;
	//var endAngle =-19* Math.PI/180;
	var counterClockwise = true;
	
	
	context1.clearRect(0, 0, canvas1.width, canvas1.height);
	
	
	var gradTemp = context1.createLinearGradient(0,0,-65,100);
	gradTemp.addColorStop(0, '#d61010');
	gradTemp.addColorStop(1, '#0fa3d1');
	
	context1.save();
	context1.beginPath();
	context1.rect(0, 0, canvas1.width, canvas1.height);
	context1.clip();
	
	context1.strokeStyle = gradTemp;
	
	
	
	context1.beginPath();
	context1.lineWidth = 25;
	context1.arc(x, y, radius, startAngle, endAngle, counterClockwise);
	
	
	// line color
	if(engineTemperature>225 && firstTime ==1){
		context1.strokeStyle = '#d61010';
		document.getElementById("tempIcon").className = "red-circle position4";
		document.getElementById("tempMeter").className = "fltleft col3 meter-right-red";
	}else{
		
		
		context1.strokeStyle = gradTemp;
		//context1.strokeStyle = '#0fa3d1';
		document.getElementById("tempIcon").className = "blue-circle position4";
		document.getElementById("tempMeter").className = "fltleft col3 meter-right";
	}
	
	context1.stroke();
	
	context1.restore();
}

function drawTemp(temp){
	if (temp>100){
	var difference = temp - engineTemperature;
	engineTemperature = temp;
	//Cancel any movement animation if a new chart is requested
	if(typeof tempAnimation_loop != undefined) clearInterval(tempAnimation_loop);
	//The animation will take 1 second
	//time for each frame is 1sec / difference in degrees
	tempAnimation_loop = setInterval(animateTemp_to,Math.round(animaDuration/difference));
	}else{
		drawTemp(101);
	}
}

function animateTemp_to(){
	//clear animation loop if degrees reaches to new_degrees
	if(animaTemp<100) 
	animaTemp = 100;
	
	if(animaTemp == engineTemperature) 
	clearInterval(tempAnimation_loop);
	if(animaTemp < engineTemperature)
		animaTemp++;
		else
		animaTemp--;
	
	initTemp();
}





//MPG
function initMPG(){
	var mpgAngle = -9-((animaMPG*48)/36)
	//var mpgAngle = -9-((mpgValue*48)/36)
	var canvas4 = document.getElementById('myCanvas4');
	var context4 = canvas4.getContext('2d');
	var x = canvas4.width / 2-76;
	var y = canvas4.height / 2+45;
	var radius = 375;
	var startAngle = -9* Math.PI/180;
	var endAngle = mpgAngle * Math.PI/180;
	// var endAngle =-57 * Math.PI/180;
	var counterClockwise = true;
	context4.clearRect(0, 0, canvas4.width, canvas4.height);
	context4.beginPath();
	context4.arc(x, y, radius, startAngle, endAngle, counterClockwise);
	context4.lineWidth = 25;
	
	// line color
	context4.strokeStyle = '#6abe45';
	context4.stroke();
}


function drawMPG(mpg){
	mpgValue = mpg;
	animaMPG = mpgValue;
	animateMPG_to();
	/*
	if(mpg>0){
	var difference = mpg - mpgValue;
	mpgValue = mpg;
	//Cancel any movement animation if a new chart is requested
	if(typeof mpgAnimation_loop != undefined) clearInterval(mpgAnimation_loop);
	//The animation will take 1 second
	//time for each frame is 1sec / difference in degrees
	mpgAnimation_loop = setInterval(animateMPG_to,Math.round(animaDuration/difference));
	}else{
		drawMPG(1);
	}*/
}

function animateMPG_to(){
	//clear animation loop if degrees reaches to new_degrees
	if(animaMPG == mpgValue) 
	clearInterval(mpgAnimation_loop);
	
	if(animaMPG < mpgValue)
		animaMPG+=0.1;
		else
		animaMPG-=0.1;
	
	initMPG();
	document.getElementById("mpgDisplay").innerHTML = Math.round(animaMPG*10)/10;		
}


//FadeAnimation
function startFades(){
	
}