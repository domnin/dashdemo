package pt.lighthouselabs.obd.reader.config;

import pt.lighthouselabs.obd.commands.protocol.ObdProtocolCommand;

/**
 * Created by dyd0912 on 9/5/2014.
 */
public class ObdATICommand extends ObdProtocolCommand {

    public ObdATICommand(String dummy) {
        super("AT I");
    }

    @Override
    public String getFormattedResult() {
        return getResult();
    }

    @Override
    public String getName() {
        return "AT I";
    }
}
